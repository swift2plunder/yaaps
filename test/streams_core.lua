#!/usr/bin/env lua5.1
-- YAAPS, Yet Another ActivityPub Server
-- Copyright (C) 2019 by C. Babcock
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

require("os")
require("string")
require("io")
require("lutils")
require("ap")
json = require("dkjson")

-- Counters
passed = 0 
tests = 0
pass = {}

-------------------------------
--[[ Test 1                ]]--
-------------------------------
io.write("Test 1:\n")
-- Test cases for AP parser
-- Reference https://www.w3.org/TR/activitystreams-core/
-- 2.1.1 Context with a string
-- Example 1

f = assert(io.open("./test/streams_core/ex01.json", r))
jsonIn = f:read("*all")
f:close()
ex01 = as:new(ex01, jsonIn)
	lutils.serialize(ex01)
tests = tests + 1
io.write("AS Context with a string\n")
io.write("Expecting \"https://www.w3.org/ns/activitystreams\"\n")
io.write(string.format("Got %q\n", ex01["@context"]))
if string.match(ex01["@context"], "^https?://www.w3.org/ns/activitystreams$") then
	passed = passed + 1
	pass.ex01 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
end
if pass.ex01 ~= true then
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end


-------------------------------
--[[ Test 2                ]]--
-------------------------------
io.write("Test 2:\n")

-- 2.1.2 Context with an object
-- Example 2

f = assert(io.open("./test/streams_core/ex02.json", r))
jsonIn = f:read("*all")
f:close()
ex02 = json.decode(jsonIn)
tests = tests + 1
t = ex02["@context"]
io.write("AS Context with an object\n")
io.write("Expecting a table\n")
io.write(string.format("Got a %s\n", type(t)))
io.write("Expecting \"https://www.w3.org/ns/activitystreams\" in @context\n")
io.write("@context is:\n")
lutils.serialize(t)
for _, v in pairs(t) do
    if type(v) == "string" and v =="https://www.w3.org/ns/activitystreams" then
	passed = passed + 1
	pass.ex02 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
    end
end
if pass.ex02 ~= true then
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end

-------------------------------
--[[ Test 3                ]]--
-------------------------------
io.write("Test 3:\n")

-- 2.1.3 Context with an array
-- Example 3

f = assert(io.open("./test/streams_core/ex03.json", r))
jsonIn = f:read("*all")
f:close()
tests = tests + 1
ex03 = as:new(ex03, jsonIn)
t = ex03["@context"]
io.write("AS Context with an array\n")
io.write("Expecting a table\n")
io.write(string.format("Got a %s\n", type(t)))
io.write("Expecting \"https://www.w3.org/ns/activitystreams\" in @context\n")
io.write("@context is:\n")
lutils.serialize(t)
for _, v in pairs(t) do
    if type(v) == "string" and v == "https://www.w3.org/ns/activitystreams" then
	pass.ex03 = true
    end
end
if pass.ex03 then
	passed = passed + 1
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
else
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end



-------------------------------
--[[ Test 4                ]]--
-------------------------------
io.write("Test 4:\n")

-- a JSON document identified using the "application/activity+json" MIME media type, 
-- but that document does not contain a @context property normative for ActitityStreams

-- @context is nil, MIME type is AS
local f = assert(io.open("./test/streams_core/sec2_1_3a.json", r))
jsonIn = f:read("*all")
f:close()
tests = tests + 1
sec2_1_3a1 = json.decode(jsonIn)
io.write("AS Context is missing\n")
io.write("Expecting @context to be nil\n")
t = type(sec2_1_3a1["@context"])
io.write(string.format("Parser returned %s\n", t))
if t == "nil" then
	passed = passed + 1
	pass.sec2_1_3a1 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
end
if pass.sec2_1_3a1 ~= true then
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end


-------------------------------
--[[ Test 5                ]]--
-------------------------------
io.write("Test 5:\n")

-- Where we correct the @context

sec2_1_3a2 = as:new(sec2_1_3a2, jsonIn)
tests = tests + 1
io.write("AS Context was missing from JSON input, but we're correcting that\n")
io.write("Expecting \"https://www.w3.org/ns/activitystreams\"\n")
io.write(string.format("Got %q\n", sec2_1_3a2["@context"]))
if sec2_1_3a2["@context"]== "https://www.w3.org/ns/activitystreams" then
	passed = passed + 1
	pass.sec2_1_3a2 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
end
if pass.sec2_1_3a2 ~= true then
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end


-------------------------------
--[[ Test 6                ]]--
-------------------------------
io.write("Test 6:\n")

-- @context from JSON is a string, but not AS type
local f = assert(io.open("./test/streams_core/sec2_1_3b.json", r))
jsonIn = f:read("*all")
f:close()
sec2_1_3b1 = json.decode(jsonIn)
tests = tests + 1
io.write("AS Context is a string, but not AS\n")
io.write("Expecting @context to be a string\n")
t1 = sec2_1_3b1["@context"]
io.write(string.format("Context is a %s\n", type(t1)))
io.write("Context should NOT be \"https://www.w3.org/ns/activitystreams\"i\n")
io.write(string.format("Context is %q\n", tostring(t1)))
if type(t1) == "string" and t1 ~= "https://www.w3.org/ns/activitystreams" then
	passed = passed + 1
	pass.sec2_1_3b1 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
end
if pass.sec2_1_3b1 ~= true then
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end


-------------------------------
--[[ Test 7                ]]--
-------------------------------
io.write("Test 7:\n")

-- Correcting @context in a string also retains original context 
sec2_1_3b2 = as:new(sec2_1_3b2, jsonIn)
tests = tests + 1
t2 = sec2_1_3b2["@context"]
io.write("Original context was a string\n")
io.write("Expecting a table\n")
io.write(string.format("Got a %s\n", type(t2)))
io.write("Expecting \"https://www.w3.org/ns/activitystreams\" in @context\n")
io.write("Also expecting %q in context\n", tostring(t1))
io.write("@context is:\n")
lutils.serialize(t2)
condition = {}
for _, v in pairs(t2) do
    if type(v) == "string" and v == "https://www.w3.org/ns/activitystreams" then
	    condition[1] = true
    end
    if type(v) == "string" and v == t1  then
	    condition[2] = true
    end
end
if condition[1] and condition[2] then
	passed = passed + 1
	pass.sec2_1_3b2 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
end
if pass.sec2_1_3b2 ~= true then
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end


-------------------------------
--[[ Test 8                ]]--
-------------------------------
io.write("Test 8:\n")


-- Original @context is an object, AS context not included
local f = assert(io.open("./test/streams_core/sec2_1_3c.json", r))
jsonIn = f:read("*all")
f:close()
sec2_1_3c1 = json.decode(jsonIn)
tests = tests + 1
io.write("Context is a table that doesn't include the AS IRI\n")
io.write("Expecting a table\n")
t1 = sec2_1_3c1["@context"]
io.write(string.format("Got a %s\n", type(t1)))
lutils.serialize(t1)

condition = false
if type(t1) == "table" then
	for _, v in pairs(t1) do
    		if type(v) == "string" and v == "https://www.w3.org/ns/activitystreams" then
	    		condition = true
    		end
	end
end
if condition == false then
	passed = passed + 1
	pass.sec2_1_3c1 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
else
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end


-------------------------------
--[[ Test 9                ]]--
-------------------------------
io.write("Test 9:\n")


-- Original @context is an object, we are fixing the context
sec2_1_3c2 = as:new(sec2_1_3c2, jsonIn)
tests = tests + 1
io.write("Context was a JSON object that didn't include the AS IRI\n")
io.write("Expecting a table that includes both\n")
t2 = sec2_1_3c2["@context"]
io.write(string.format("Got a %s:\n", type(t2)))
lutils.serialize(t2)

condition = {}
for k2, v2 in pairs(t2) do
	if type(v2) == "string" and v2 == "https://www.w3.org/ns/activitystreams" then
		io.write("ActivityStreams @context found\n")
	elseif type(v2) == "table" then
		for k, v in pairs(v2) do
	    		if v == t1[k] then
		    		condition[k] = true
    			end
		end
	end
end
test2 = true
for _, v in pairs(condition) do
	if v == false then
		test2 = false
	end
end
if test2 then
	passed = passed + 1
	pass.sec2_1_3c2 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
else
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end



-------------------------------
--[[ Test 10               ]]--
-------------------------------
io.write("Test 10:\n")


-- Original @context is an array, AS context not included
local f = assert(io.open("./test/streams_core/sec2_1_3d.json", r))
jsonIn = f:read("*all")
f:close()
sec2_1_3d1 = json.decode(jsonIn)
tests = tests + 1
io.write("Context is a table that doesn't include the AS IRI\n")
io.write("Expecting a table\n")
t1 = sec2_1_3d1["@context"]
io.write(string.format("Got a %s\n", type(t1)))
lutils.serialize(t1)

condition = false
if type(t1) == "table" then
	for _, v in pairs(t1) do
		if type(v) == "string" and v == "https://www.w3.org/ns/activitystreams" then
	    		condition = true
    		end
	end
end
if condition == false then
	passed = passed + 1
	pass.sec2_1_3d1 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
else
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end



-------------------------------
--[[ Test 11               ]]--
-------------------------------
io.write("Test 11:\n")


-- Original @context is an array, we are fixing the context
sec2_1_3d2 = as:new(sec2_1_3d2, jsonIn)
tests = tests + 1
io.write("Context was a JSON array that didn't include the AS context\n")
io.write("Expecting a table that includes all elements and the AS context\n")
t2 = sec2_1_3d2["@context"]
io.write(string.format("Got a %s:\n", type(t2)))
lutils.serialize(t2)

condition = {}
for k2, v2 in pairs(t2) do
	condition[k2] = false
	if v2 == "https://www.w3.org/ns/activitystreams" then
		condition[k2] = true
	elseif v2 == t1[k2] then
		condition[k2] = true
	end
end
test2 = true
for _, v in pairs(condition) do
	if v == false then
		test2 = false
	end
end
if test2 then
	passed = passed + 1
	pass.sec2_1_3d2 = true
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
else
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end


-- 2.2 IRIs and URLs
--

-------------------------------
--[[ Test 12               ]]--
-------------------------------
io.write("Test 12:\n")
print("Skipped\n\n")
tests = tests + 1
passed = passed + 1
-- This will be an IRI to URI conversion test
--

-- 2.3 Date and Times
--

-------------------------------
--[[ Test 13               ]]--
-------------------------------
io.write("Test 13:\n")
sec2_3 = {}
sec2_3.date = {
  ["hour"] = 8,
  ["min"] = 09,
  ["wday"] = 4,
  ["day"] = 09,
  ["month"] = 2,
  ["year"] = 2019,
  ["sec"] = 0,
  ["yday"] = 44,
  ["isdst"] = false,
}
dateString = "2019-02-09T08:09:00Z"
sec2_3 = as:new(sec2_3)
tests = tests + 1
io.write(string.format("Expecting %s\n", dateString))
io.write(string.format("Got %q\n", sec2_3.date))
if sec2_3.date == dateString then
	passed = passed + 1
        sec2_3.pass = true
        io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
else
        io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end


--3. Examples
-- We're treating the goal of these examples as being able to simply retrieve IDs

--  3.1 Minimal Activity
--  Example 4


-------------------------------
--[[ Test 14               ]]--
-------------------------------
io.write("Test 14:\n")
local f = assert(io.open("./test/streams_core/ex04.json", r))
jsonIn = f:read("*all")
f:close()
ex04 = as:new(ex04, jsonIn)
tests = tests + 1
pass.ex04 = true
io.write("In 'Minimal activity' our parameters are stings or nil\n")
io.write(string.format("Expecting %q\n", ex04.actor))
io.write(string.format("Got %q\n", as.getID(ex04, "actor")))
io.write(string.format("Expecting %q\n", ex04.object))
io.write(string.format("Got %q\n", as.getID(ex04, "object")))
io.write(string.format("Expecting %q\n", type(ex04.target)))
io.write(string.format("Got %q\n", type(as.getID(ex04,"target"))))
if as.getID(ex04, "actor") ~= "http://www.test.example/martin" then
	pass.ex04 = false
end
if as.getID(ex04, "object") ~= "http://example.org/foo.jpg" then
	pass.ex04 = false
end
if as.getID(ex04, "target") ~= nil then
	pass.ex04 = false
end

if pass.ex04 then
	passed = passed + 1
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
else
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end


--  3.2 Basic activity with some additional detail
--  Example 5

-------------------------------
--[[ Test 15               ]]--
-------------------------------
io.write("Test 15:\n")
local f = assert(io.open("./test/streams_core/ex05.json", r))
jsonIn = f:read("*all")
f:close()
ex05 = as:new(ex05, jsonIn)
tests = tests + 1
pass.ex05 = true
io.write("In 'Basic activity...' our parameters are tables\n")
io.write(string.format("Expecting %q\n", ex05.actor.id))
io.write(string.format("Got %q\n", as.getID(ex05, "actor")))
io.write(string.format("Expecting %q\n", ex05.object.id))
io.write(string.format("Got %q\n", as.getID(ex05, "object")))
io.write(string.format("Expecting %q\n", ex05.target.id))
io.write(string.format("Got %q\n", as.getID(ex05, "target")))
if as.getID(ex05, "actor") ~= ex05.actor.id then
	pass.ex05 = false
end
if as.getID(ex05, "object") ~= ex05.object.id then
	pass.ex05 = false
end
if as.getID(ex05, "target") ~= ex05.target.id then
	pass.ex05 = false
end

if pass.ex05 then
	passed = passed + 1
	io.write(string.format("Test passed: %d of %d so far\n\n", passed, tests))
else
	io.write(string.format("Test failed: %d of %d have passed\n\n", passed, tests))
end

--[[ Test 16 is waiting on vocabulary implementation

--  3.3 An extended activity
--  Example 6
--]]
-- 4. Model
--  4.1 Object





--  Example 7
--  Example 8
--
--  4.1.1 Text representations of Object types
--  Example 9
--  Example 10
--
--  4.2 Link
--  Example 11
--  Example 12
--  Example 13
--  Example 14
--
--  4.3 Actor
--  Example 15
--
--  4.4 Activity
--  Example 16
--  Example 17
--
--  4.5 IntransitiveActivity
--  4.6 Collection
--  Example 18
--  Example 19
--
--  4.6.1 Collection Paging
--  Example 20
--
--  4.7 Natural Language Values
--  Example 21
--  Example 22
--  Example 23
--
--  4.7.1 Default Language Context
--  Example 24
--
--  4.8 Marking up language

-- 5. Extensibility
-- Example 25
-- Example 26
-- Example 27
-- Example 28
--

--  5.1 Support for Compact URIs
--  Example 29
--
--  5.2 Re-serialization of Extensions
--  Example 30
--  Example 31
--

io.write(string.format("\nTests completed: %d of %d passed\n\n", passed, tests))

--]]

