-- YAAPS, Yet Another ActivityPub Server
-- Copyright (C) 2019 by C. Babcock
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

--


pase = {}

-- Built in libraries explicitly included
require("string")
require("os")
require("io")

-- Our local utilities
require("lutils")
require("ap")
config = require("config") or require("config_example")

-- Object definitions
pase.Object = ap.Object:new()
pase.Link = ap.Link:new()

pase.Actor = lutils.createClass(pase.Object, ap.Actor)

pase.Activity = lutils.createClass(pase.Object, ap.Activity)
pase.IntransitiveActivity = lutils.createClass(pase.Activity, ap.IntransitiveActivity)

pase.Accept = lutils.createClass(pase.Activity, ap.Accept)
pase.Add = lutils.createClass(pase.Activity, ap.Add)
pase.Announce = lutils.createClass(pase.Activity, ap.Announce)
pase.Arrive = lutils.createClass(pase.Activity, ap.Arrive)
pase.Block = lutils.createClass(pase.Activity, ap.Block)
pase.Create = lutils.createClass(pase.Activity, ap.Create)
pase.Delete = lutils.createClass(pase.Activity, ap.Delete)
pase.Dislike = lutils.createClass(pase.Activity, ap.Dislike)
pase.Flag = lutils.createClass(pase.Activity, ap.Flag)
pase.Follow = lutils.createClass(pase.Activity, ap.Follow)
pase.Ignore = lutils.createClass(pase.Activity, ap.Ignore)
pase.Invite = lutils.createClass(pase.Activity, ap.Invite)
pase.Join = lutils.createClass(pase.Activity, ap.Join)
pase.Leave = lutils.createClass(pase.Activity, ap.Leave)
pase.Like = lutils.createClass(pase.Activity, ap.Like)
pase.Listen = lutils.createClass(pase.Activity, ap.Listen)
pase.Move = lutils.createClass(pase.Activity, ap.Move)
pase.Offer = lutils.createClass(pase.Activity, ap.Offer)
pase.Question = lutils.createClass(pase.Activity, ap.Question)
pase.Reject = lutils.createClass(pase.Activity, ap.Reject)
pase.Read = lutils.createClass(pase.Activity, ap.Read)
pase.Remove = lutils.createClass(pase.Activity, ap.Remove)
pase.TentativeAccept = lutils.createClass(pase.Accept, ap.TentativeAccept)
pase.TentativeReject = lutils.createClass(pase.Reject, ap.TentativeReject)
pase.Travel = lutils.createClass(pase.Activity, ap.Travel)
pase.Undo = lutils.createClass(pase.Activity, ap.Undo)
pase.Update = lutils.createClass(pase.Activity, ap.Update)
pase.View = lutils.createClass(pase.Activity, ap.View)


pase.Article = lutils.createClass(pase.Object, ap.Article)
pase.Audio = lutils.createClass(pase.Object, ap.Audio)
pase.Document = lutils.createClass(pase.Object, ap.Document)
pase.Event = lutils.createClass(pase.Object, ap.Event)
pase.Image = lutils.createClass(pase.Object, ap.Image)
pase.Note = lutils.createClass(pase.Object, ap.Note)
pase.Page = lutils.createClass(pase.Object, ap.Page)
pase.Place = lutils.createClass(pase.Object, ap.Place)
pase.Profile = lutils.createClass(pase.Object, ap.Profile)
pase.Relationship = lutils.createClass(pase.Object, ap.Relationship)
pase.Tombstone = lutils.createClass(pase.Object, ap.Tombstone)
pase.Video = lutils.createClass(pase.Object, ap.Video)
                                  
pase.Mention = lutils.createClass(pase.Link, ap.Mention)


-- Actors
pase.Application = lutils.createClass(pase.Actor, ap.Application)
pase.Group = lutils.createClass(pase.Actor, ap.Group)
pase.Organization = lutils.createClass(pase.Actor, ap.Organization)
pase.Person = lutils.createClass(pase.Actor, ap.Person)
pase.Service = lutils.createClass(pase.Actor, ap.Service)


-- Collections
pase.Collection = lutils.createClass(pase.Object, ap.Collection)
pase.OrderedCollection = lutils.createClass(pase.Collection, ap.OrderedCollection)
pase.CollectionPage = lutils.createClass(pase.Collection, ap.CollectionPage)
pase.OrderedCollectionPage = lutils.createClass(pase.OrderedCollection, pase.CollectionPage, ap.OrderedCollectionPage)

pase.inbox = pase.OrderedCollection:new()
pase.outbox = pase.OrderedCollection:new()

-- Utility functions


-- Client to Server Interactions
-- Server to Server Interactions

