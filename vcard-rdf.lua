-- YAAPS, Yet Another ActivityPub Server
-- Copyright (C) 2019 by C. Babcock
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This file will provide vcard-rdf support 
-- for Person, Group, and Organization support.

-- The absence of code here indicates 
-- that the code is not ready for end user accounts

