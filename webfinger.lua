#!/usr/bin/env lua5.1
-- YAAPS, Yet Another ActivityPub Server
-- Copyright (C) 2019 by C. Babcock
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

require("os")
require("string")

local hex={}
for i=0,255 do
   hex[string.format("%0x",i)]=string.char(i)
   hex[string.format("%0X",i)]=string.char(i)
end

local function decodeURI(s)
   return (s:gsub('%%(%x%x)',hex))
end


if string.match( os.getenv( "REQUEST_METHOD" ), "^GET$") == nil then
   io.write( "Status: 405 'Method Not Allowed'\n" )
   os.exit(0)
end

query = os.getenv("QUERY_STRING")
resource = string.match( query, "resource=([^&]+)" ) 
subject = decodeURI( string.lower( resource ) )
scheme, luser, domain = string.match( subject, "([^:]+):(.+)@(.+)" )

if scheme == "acct" then
   dofile( string.format( './%s/acct.lua', domain ) )
end

io.write( acct[luser] )


