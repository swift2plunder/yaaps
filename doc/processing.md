# Processing

This is an overview of the YAAPS processing model

## C2S - (An Object POSTed to an Outbox)

The only lexical difference between ActivityPub S2S and C2S is that
S2S *must* provide an Activity, while C2S may publish any Object,
regardless of whether it's an Activity. C2S is distinguishable from
S2S by the fact that it is received on an Outbox endpoint rather than
an Inbox endpoint. YAAPS is intended to provide a demonstration of
interoperability with federated ActivityPub implementations that may
or may not implement the C2S protocol

Access to push operations or document creation that has side
effects (like federating the created document to other servers)
requires authenticated access to the Outbox of an Actor. ActivityPub
C2S implementations are required to support an authentication
method

OAuth 2 is the prevailing mechanism for interacting with ActivityPub
services. Mastodon and other implementations utilize server tokens for
interaction with external content, which creates the requirement for
the implementation to be an issuer of tokens when hosting identities

YAAPS will defer implementation of OAuth 2 for as long as
possible. (The Lua libraries I've found aren't current and I'm not
interested in the implementation)

In order to provide outbox capabilities to those accessing these
capabilities through ActivityPub services that don't implement AP C2S,
YAAPS will provide capability inboxes (below) that can escalate a
message to an outbox based on criteria established by the owner of the Actor 

## S2S - (An Activity POSTed to an Inbox)

### Shared Inbox

The shared inbox is discoverable through Webfinger

Webfinger support is included to provided compatiblity with
Mastodon. The well-known/.webfinger endpoint will point the querent to
a shared inbox. YAAPS will treat Activities posted to the shared inbox with
~~ the contempt they deserve~~ a regard to Mastodon compatibility

Items posted to the shared inbox are promoted to individual inboxes
according to conventions consistent with the expectations of the
Mastodon community when possible and with the actual behavior of
Mastodon otherwise

### Default Inbox

The default inbox is discovered through a response from an actor to an
unprivileged query or reachable through the shared inbox

Documents received at the default inbox are persisted without side effects such
as notifications. The owner of the inbox can provide filters or enable
shared filters to escalate messages to inboxes with capabilities or to
inform views of the inbox

### Capability Inboxes

YAAPS will create Capability inboxes with specific permissions or
processing specified. These inboxes may be accessed by promotion from the
default inbox or through capability URLs. Capability URLs may be
attached to objects published by the Actor or from authenticated
queries of the Actor where the Actor's owner has provided YAAPS with a
profile for delegating permission and behaviors for the querent's
posts

Capability inboxes provide the means for the owner of an Actor to
specify behavior for messages at an inbox and to allow delegation of
permission to perform those behaviors withough giving third parties
access to the delegation mechanism. This allows for features like per
user notifications on replies to a thread or allowing those followed
by the Actor to move servers and Update their identity in the
Following collection

One role of Capability Inboxes in YAAPS will be to programatically
interact with an Outbox based on the properties of a message. For
example, a Note Object attributedTo an Actor on a Mastodon instance is
received in the shared inbox addressed to
@msg+g4.alliance.AlliedCoopers@tag.asciiking.com. The shared inbox
at tag.asciiking.com verifies the HTTP signature and submits it to an
inbox for @msg. The inbox looks up the attributedTo, rewrites the
message envelope, and routes it to an Outbox. In this example, TAG is
the game "The Age of Gods," g4 is game 4, and AlliedCoopers is an
alliance (Group Actor). Typical rules for such a message might be that
the sender must be in game 4 and the message will go to all member of
the alliance

## Logging and Persistence

### Raw log

The raw log is an mbox file where incoming ActivityStreams activity is
stored with information from the HTTP environment in an RFC 822
format. The raw log is intended for debugging purposes only and can be
safely deleted. This is an implementation feature

### Create log

The Create log is an append-only list of Activities processed by
YAAPS. In this log, first children of referrenced objects are expanded so that
processing can begin at any point in the log and, continuing to the
end, produce a copy of the server contents that includes all objects
referenced since that entry. This can be used to clone a server's
history, in whole or in part, but its primary purpose is to provide
consistent, reproducible state for processing. 

## Libraries

>!The libraries for ActivityStreams, ActivityPub, and any additional
>!extensions provide a mechanism for traversing a JSON object
>!or array, like the Create log, according to the semantics of the
>!protocols. The original intention was to use the ActivityPub
>!implementation as an example of how to implement an ActivityStreams
>!extension, but there may be some refactoring required to accompllish that
>!
>!ActivityPub introduces additional processing requirements for objects
>!in the ActivityStreams name space. As currently implemented, an implementation using
>!ActivityPub should use the AP library, which references as.lua
>!internally. It's possible that another extension, such as LitePub,
>!might further refine objects in the main name space
>!
>!The protocol library builds a consistent state object, based on the
>!arguement it is given, by traversing the object and calling the PASE
>!library to retrieved objects embedded by reference. The object
>!returned is a consistent 
>!
>!The PASE library retrieves requested objects from local or remote
>!
>!The Persistence And Side Effects library provides the interface
>!between the protocol libraries and the external environment. The PASE
>!in YAAPS is currently being written to target a conventional file
>!system on Linux and rely on OS mechanisms for concurrency
