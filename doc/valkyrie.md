# Valkyrie Storage API **unstable**

Based on Versium, originally by Yuri Takhteyev

https://github.com/swift2plunder/versium
http://sputnik.freewisdom.org/en/Versium

Valkyrie is a simple storage API used by YAAPS and Freya based on
Versium from the Sputnik wiki. The main diffence between Versium and
Valkyrie is that Versium returned completed nodes because it was
created for wiki storage, where a node represented a document. Valkyrie
nodes represent objects, such as ActivityPub Activities, that transform
a stream. Valkyrie nodes are deltas.

Each Valkyrie implementation is a Lua module that can be loaded with
"require".

YAAPS is a lightweight ActivityPub implementation for experimentation
and hosting federated services.

Freya is a data adapter to maintain data consistency in object streams
when component objects may require access via diverse schema.

## Capabilities

Since many parts of the API are optional, each implementation must define a table that specifies what it does and does not support. 

For instance:

```lua
capabilities = { 
can_save = true, -- an implementation can be read-only 
allow_changes = true, -- an implementation may not be able to change the contents of an existing node 
has_history = true, -- some implementations don't keep track of 
history is_persistent = true, -- we'll still have the data if the server goes down 
supports_extra_fields = true, -- revisions can have fields other than those required 
}
```

The allow_changes property is an important addition to Versium for
Valkyrie that is intended to allow Valkyrie plugins to support p2p
file systems. Contributions and suggestions for implementing Secure
Scuttlebutt and Dat plugins are welcome. 

## Creating a new instance

A new instance of the implementation is created by a function called "new()". This function can take parameters, which are implementation-specific. Note that new() does not create any storage system (such as a database). It creates an Lua object that represents a connection to a storage system.

For instance:

```lua
local valkyrie_implementation = require(module_name)
local my_valkyrie_instance = valkyrie_implementation.new(params)
```

The argument "params" is a table, the contents of which are
specific to the plugin implementation

## Methods

This is an overview of methods for implementers, but a more complete
resource is required. The Freya library is likely the primary consumer of
this API for the foreseeable future, so these methods are not likely
to be used directly by other software.

### Node Exists
Once the client creates an instance, it interacts with the storage
system using this instance.

```lua
node_exists(id)
```

Returns true or false depending on whether there is a node with this
ID.

### Get Node History

This Versium method is not part of the Valkyrie API. In Valkyrie, a
node represents a delta, not a version.

### Get Node Info

```lua
get_node_info(id)
```
Returns the data stored in the node as a table. Returns nil if the node doesn't exist. Throws an error if anything else goes wrong.

```lua
data = get_node_info("Foo")
```

After that "data" will be a table set to the content of the node in the format supplied at node creation.

### Get Node

Returns the contents of the data attribute of the node as a text string

```lua
data = get_node("Foo")
```

### Create Node

Creates a node with the supplied parameters and returns the id

```lua
id = create_node(params, save)
```

The "params" are those required by the instance as defined at
creation of the instance. The "save" is a boolean describing whether
the object is immediately persisted.

### Get Node IDs - **TO-DO**

This is a paged interface for fetching nodes according to a
criteria. It would probably be a handy feaure to have, but it's
important to establish an understanding of indexing structures
available in more of the target media before designing this interface.
