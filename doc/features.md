Features
========

YAAPS is intended to serve as a compact and customizable AP implementation to accomodate the needs of serving game notification, conducting technology demos, and providing simple bridges to other networks

Separation of concerns in YAAPS is rigidly enforced in order to provide a manageable surface area for modders

ActivityStreams, and by extension ActivityPub, is processed as a Turing Complete language

YAAPS will permit clients, including remote clients, to send Types normally sent as a response as an anticipation of receiving content. A simple application of this facility would be an "out of office" message
