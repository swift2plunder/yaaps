Opinions
========

I started YAAPS as an "unopinionated" implementation, qualified it as "minimally" opinionated, and eventually realized that avoiding opinions was less of a virtue than a recipe to avoid introspection. This document represents the opinion that introspection is a useful process for determining how tightly opinions might be held without damage as well as for crafting implementation strategies

ActivityStreams 2.0 is a programming language
---------------------------------------------
The MIME type for AS is "application." The specs describe a syntax tree written in Object Notation. Treating AS as a programming environment in implementation does not expose new vulnerabilities because our threat profile consists of actors using conventional programming models to federate undesired content (spam, freeze peaches). Rather, conceptualizing our implementation as processing a computing language provides patterns for improving our implementation from basic considerations such as garbage collection and the structure of conditional expressions to more advanced ideas such as object capabilities and reactive programming



Backwards 
