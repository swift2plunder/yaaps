End points
==========

This is a description of the provided access points

Webfinger
---------

Webfinger is provided for Mastodon compatibility. The minimal implementation for this is describe in this blog:

https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/

Inbox
-----
All inboxes in YAAPS are implemented in a single file.

HTTP POST to an inbox represents a federated message inbound to the owner

HTTP GET from an authenticated client retrieves those messages

A design goal of YAAPS is to provide programmatic control of the inbox

Outbox
------

HTTP POST to an Outbox represents an outbound massage for the service to handle.

HTTP GET from an Outbox represent a query into the materials published there


