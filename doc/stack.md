# YAAPS Stack

Onions got layers, Ogres got layers, and YAAPS got layers.

## Valkyrie - **in progress**

Valkyrie is an object storage API that provides a mechanism for low level data access (networked and local object stores) to inform the software calling it of its capabilities and limitations. Use Valkyrie to retrieve an individual node with references to first children. 

## Freya

Freya is a versioned object store for ActivityPub that uses a configuration file to map available schemas to Valkyrie plugins. Freya maintains indexes on node relationships and treats Activities as deltas in an ActivityStream. Use Freya to retrieve a provably consistent expanded Activity Stream.

## Persistance And Side Effects - **in progress**

The PASE module provides storage and replication capabilities that implementations can bind to objects upon construction. PACE methods rely on Freya to provide a consistent copy of relevant state and to manage storage of new nodes.

## Protocol Libraries - **available**

The protocol libraries use prototype-based inheritance to describe the relationships among Types in ActivityStreams and extensions like ActivityPub. ActivityPub is implemented as an ActivityStreams extension in order to demonstrate how to build and link extension libraries. When an implementation binds a method to an object type defined in the protocol libraries, the method is inherited according to relationships described with the word "extends" (e.g. "Activity extends Object") in the protocol specifications.

## Template Libraries

The Template Libraries are a set of Activity prototypes that can be referenced in ActivityPub C2S to facilitate common tasks

## Admin Actor

The admin actor is a local extension of the person actor initialized with the capabilities required to bootstrap operations

## Interoperability Features

Additional facilities such and webfinger, http signatures, and OAuth2 are required in order to interoperate with Mastodon and other Fediverse services. Decisions will be made on incorporating these ancillary protocols on an individual basis. Pull requests are encouraged.
