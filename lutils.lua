-- The contents of this file are exempt from the requirements of AGPL
--
-- This code represents solutions common to the community
-- being derived from examples in The Lua Manual, Programing in Lua,
-- and advice given in chatrooms and help forums.
--
-- Any judge hearing suit that depends on copyright of these functions
-- is encouraged to treat such, and those involved,
-- with the contempt that they deserve



lutils = {} 

-- Built in libraries explicitly included
require("string")
require("os")
require("io")

-- This loads our table for decoding URIs
local hex={}
for i=0,255 do
   hex[string.format("%0x",i)]=string.char(i)
   hex[string.format("%0X",i)]=string.char(i)
end


-- Our URI decoding function doesn't tolerate mixed case hex
function lutils.decodeURI(s)
   return (s:gsub('%%(%x%x)',hex))
end

-- Serialize Lua table
-- Takes an object and an optional file handle
function lutils.serialize (o, fh)
	if fh == nil then
		fh = io.output()
	end
	if type(o) == "number" then
      	fh:write(o)
	elseif type(o) == "string" then
      	fh:write(string.format("%q", o))
	elseif type(o) == "table" then
      	fh:write("{\n")
	      for k,v in pairs(o) do
			if type(v) ~= "Function" then
	      		fh:write("  [")
      			lutils.serialize(k, fh)
	      		fh:write("] = ")
				lutils.serialize(v, fh)
	         		fh:write(",\n")
		   	end
			fh:write("}\n")
		end
	elseif type(o) == "boolean" then
		if o then 
			fh:write("true") 
		else fh:write("false") 
		end
	elseif type(o) == "nil" then
		fh:write("nil\n")
	else
      	fh:write(string.format("  -- %q removed \n", type(o)))
		o = nil
	end
	return o
end


-- look up for `k' in list of tables `plist'
local function search (k, plist)
	for i=1, table.getn(plist) do
		local v = plist[i][k]    -- try `i'-th superclass
		if v then return v end
	end
end

function lutils.createClass (...)
     local c = {}      -- new class
   
     -- class will search for each method in the list of its
     -- parents (`arg' is the list of parents)
     setmetatable(c, {__index = function (t, k)
      return search(k, arg)
     end})
   
     -- prepare `c' to be the metatable of its instances
     c.__index = c
   
     -- define a new constructor for this new class
     function c:new (o)
      o = o or {}
      setmetatable(o, c)
      return o
     end
   
     -- return new class
     return c
end


