-- YAAPS, Yet Another ActivityPub Server
-- Copyright (C) 2019 by C. Babcock
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- ActivityPub and ActivityStreams are frequently conflated
-- YAAPS separates AP and AS specifications into separate libraries
-- Since AP is an AS extention, the AP library can be a model 
-- for building other extensions


ap = {}

-- Built in libraries explicitly included
require("string")
require("os")
require("io")

-- Our local utilities
require("lutils")
require("as")


-- Object definitions
ap.Object = as.Object:new()
ap.Link = as.Link:new()

ap.Actor = lutils.createClass(ap.Object, as.Actor)

ap.Activity = lutils.createClass(ap.Object, as.Activity)
ap.IntransitiveActivity = lutils.createClass(ap.Activity, as.IntransitiveActivity)

ap.Accept = lutils.createClass(ap.Activity, as.Accept)
ap.Add = lutils.createClass(ap.Activity, as.Add)
ap.Announce = lutils.createClass(ap.Activity, as.Announce)
ap.Arrive = lutils.createClass(ap.Activity, as.Arrive)
ap.Block = lutils.createClass(ap.Activity, as.Block)
ap.Create = lutils.createClass(ap.Activity, as.Create)
ap.Delete = lutils.createClass(ap.Activity, as.Delete)
ap.Dislike = lutils.createClass(ap.Activity, as.Dislike)
ap.Flag = lutils.createClass(ap.Activity, as.Flag)
ap.Follow = lutils.createClass(ap.Activity, as.Follow)
ap.Ignore = lutils.createClass(ap.Activity, as.Ignore)
ap.Invite = lutils.createClass(ap.Activity, as.Invite)
ap.Join = lutils.createClass(ap.Activity, as.Join)
ap.Leave = lutils.createClass(ap.Activity, as.Leave)
ap.Like = lutils.createClass(ap.Activity, as.Like)
ap.Listen = lutils.createClass(ap.Activity, as.Listen)
ap.Move = lutils.createClass(ap.Activity, as.Move)
ap.Offer = lutils.createClass(ap.Activity, as.Offer)
ap.Question = lutils.createClass(ap.Activity, as.Question)
ap.Reject = lutils.createClass(ap.Activity, as.Reject)
ap.Read = lutils.createClass(ap.Activity, as.Read)
ap.Remove = lutils.createClass(ap.Activity, as.Remove)
ap.TentativeAccept = lutils.createClass(ap.Accept, as.TentativeAccept)
ap.TentativeReject = lutils.createClass(ap.Reject, as.TentativeReject)
ap.Travel = lutils.createClass(ap.Activity, as.Travel)
ap.Undo = lutils.createClass(ap.Activity, as.Undo)
ap.Update = lutils.createClass(ap.Activity, as.Update)
ap.View = lutils.createClass(ap.Activity, as.View)


ap.Article = lutils.createClass(ap.Object, as.Article)
ap.Audio = lutils.createClass(ap.Object, as.Audio)
ap.Document = lutils.createClass(ap.Object, as.Document)
ap.Event = lutils.createClass(ap.Object, as.Event)
ap.Image = lutils.createClass(ap.Object, as.Image)
ap.Note = lutils.createClass(ap.Object, as.Note)
ap.Page = lutils.createClass(ap.Object, as.Page)
ap.Place = lutils.createClass(ap.Object, as.Place)
ap.Profile = lutils.createClass(ap.Object, as.Profile)
ap.Relationship = lutils.createClass(ap.Object, as.Relationship)
ap.Tombstone = lutils.createClass(ap.Object, as.Tombstone)
ap.Video = lutils.createClass(ap.Object, as.Video)
                                  
ap.Mention = lutils.createClass(ap.Link, as.Mention)


-- Actors
ap.Application = lutils.createClass(ap.Actor, as.Application)
ap.Group = lutils.createClass(ap.Actor, as.Group)
ap.Organization = lutils.createClass(ap.Actor, as.Organization)
ap.Person = lutils.createClass(ap.Actor, as.Person)
ap.Service = lutils.createClass(ap.Actor, as.Service)


-- Collections
ap.Collection = lutils.createClass(ap.Object, as.Collection)
ap.OrderedCollection = lutils.createClass(ap.Collection, ap.OrderedCollection)
ap.CollectionPage = lutils.createClass(ap.Collection, ap.CollectionPage)
ap.OrderedCollectionPage = lutils.createClass(ap.OrderedCollection, ap.CollectionPage, as.OrderedCollectionPage)

ap.inbox = ap.OrderedCollection:new()
ap.outbox = ap.OrderedCollection:new()
ap.followers = ap.Collection:new()
ap.following = ap.Collection:new()
ap.liked = ap.OrderedCollection:new()
ap.likes = ap.OrderedCollection:new()
ap.shares = ap.OrderedCollection:new()
ap.streams = ap.Collection:new()



-- Client to Server Interactions

function ap.Object.wrap(self)
    local o = self
    if self.isObject == true and self.isActivity == nil then
        -- TODO Write Create wrapper
    end
    return self
end


-- Server to Server Interactions

