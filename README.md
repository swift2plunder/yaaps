YAAPS - Yet Another ActivityPub Server
======================================

YAAPS is a minimal AP server in Lua 5.1 by C.Babcock
Contact on the Fediverse:
	@yaaps@banana.dog
	@AceNovo@Kitty.Town
	@AceNovo@AuntieFaSupply.Co

YAAPS runs in a CGI environment, not mod\_lua or WSAPI.

The test environment is Ubuntu 18.4LTS, Apache 2.4, mod\_cgi, and Lua 5.1. Patches will be accepted for interoperability with other Linux environments.

Lua 5.1 and CGI was specifically chosen as a development environment for the stability of the platform and the paucity of libraries. The lack of safety and stability in the NodeJS ecosystem and the exposure of GoLang to "benign" surveillance by Google when deployed on their infrastructure were contributing factors to this decision, but the primary motivation is ease of use of Lua in combination with games written in C that I maintain.

Minimal server means that YAAPS is _not_ a Mastodon replacement. All we're doing in the core service is verifying incoming AP, decoding the JSON, serializing the resulting Lua table, and storing it in the file system to serve later. There is no HTML interface currently on the roadmap. My use case is implementing games as services that send notifications to players on AP servers like Pleroma and Mastodon. My initial client connections will be ActivityPub services. Untrusted "person" accounts should not be allowed until the policy administration module is ready.

This leads us to structure. This server is deliberately naive in order to minimize load on the maintainer. Authentication and authorization, media handling, extentions, and anything else not covered above will be handled by external services.

The tests are keyed to the spec and are intended to provide a description of a test suite for generic AP implementations. The functions in lutils.lua are idiomatic Lua for common tasks. The AP and AS libraries are in as.lua and ap.lua.

Further discussion of the motivation for this project and the differences between similar projects can be found at https://banana.dog/web/statuses/101564368587000870

Roadmap
=======

Release 0.1 - WIP. The server release. Implement the ActivityStreams and ActivityPub specifications, WebFinger and HTTP Signatures, and OAuth. Demonstrate interoperability with Golem, Mastodon, Pleroma, NextCloud, and any targets of opportunity.

Release 0.2 - The library release. AP and AS libraries released separately. Test cases complete.

Release 0.3 - The client release. Add a Linux client so applications can send notifications from the command line.

Interlude - Re-release 4 games that previously relied on email to communicate with players.

Release 0.4 - The person release. Add WebAuthN with an OCap IRI implementation for creating accounts for people in the server and client. For trusted users only.

Release 0.5 - The graphical client release. Sceduled to be developed with Qt and Lua. Test devices are Ubuntu with KDE Plasma, various Android phones, and whatever volunteers supply.

Release 0.6 - The administrator release. Reasonable defaults for administrative functions and the ability to mess them up through the client interfaces.

Release 0.7 - The customizer release. Audit the codebase for hardcoded default behaviors and expose those to maintainers via the configuration.

Interlude - Various demos, including flexible storage, local behaviors, and permissions for non-local user.

Release 0.8 - Security audit and hardening. May introduce some inconveniences.

Release 0.9 - The test release. Brave individuals are encouraged to run instances.


