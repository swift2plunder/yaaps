-- YAAPS, Yet Another ActivityPub Server
-- Copyright (C) 2019 by C. Babcock
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- ActivityPub and ActivityStreams are frequently conflated
-- YAAPS separates AP and AS specifications into separate libraries
-- Since AP is an AS extention, the AP library can be a model 
-- for building other extensions

as = {}

-- Built in libraries explicitly included
require("string")
require("os")
require("io")

-- Our local utilities
-- require("lutils")

json = require("yaml")


function as.new(self, o, jsonIn)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   if jsonIn then
      o = json.decode(jsonIn)
   end
   o = as.fixContext(o)
   o = as.date(o)
   return o
end



function as.getID(o, param)
   -- Returns the id of the given parameter as a string if it exists
   if param == nil then
      return o.id
   end
   t = type(o[param])
   if t == "string" or t == "nil" then
      return o[param]
   elseif t == "table" then
      return o[param].id
   else return nil
   end
end


function as.fixContext(o)
   o = o or {}
   -- This ladder of conditions exists to check whether the AS @context is already present
   if o["@context"] == nil then
      o["@context"] = "https://www.w3.org/ns/activitystreams"
      return o
   elseif type(o["@context"]) == "string" then
      if o["@context"] == "https://www.w3.org/ns/activitystreams" or
          o["@context"] == "http://www.w3.org/ns/activitystreams" then
         return o
      else 
         local tmp = o["@context"]
         o["@context"] = {"https://www.w3.org/ns/activitystreams", tmp}
         return o
      end
   -- We'll append the AS context to table
   elseif type(o["@context"]) == "table" then
      -- If any key isn't a number, the table is an object
      local tableType = "array"
      local i = 1
      for k in pairs(o["@context"]) do
         i = i + 1
         if type(k) ~= "number" and type(k) ~= "nil" then
            tableType = "object"
         end
      end
      -- Adding the AS context varies depending on the type of table
      if tableType == "array" then
         -- Our counter is at one beyond the end of the table
         o["@context"][i] = "https://www.w3.org/ns/activitystreams"
         return o
      end
      if tableType == "object" then
         local tmp = o["@context"]
         o["@context"] = {"https://www.w3.org/ns/activitystreams", tmp}
         return o
      end
   else
      -- If this code ever runs, something is very wrong
      o["@context"] = "https://www.w3.org/ns/activitystreams"
      return o
   end
   -- redundant
   return o
end


-- Date function
-- Returns with an RFC 3339 date string formated to AS requirements
-- t1 is a table such as produced by os.date("*t")
function as.date(o, t1)
   o = o or {}
   t1 = t1 or o.date or os.date("!*t")
   local t2 = {}
   if type(t1) == "string" then
      -- Assume the date is already in AP rfc3339 format
      -- TODO - Assume it isn't
      o.date = t1
      return o
   end
   if type(t1) ~= "table" then
      return nil, "Argument supplied is not a table or a string" 
   end
   -- AS Core requires 4 digit year
   if t1.year and #tostring(t1.year) == 4 then
      -- TODO support additional cases as needed
      t2.fullyear = tostring(t1.year)
   else return nil, "Format for year not supported" 
   end
   if t1.month then
      if type(t1.month) == "number" and t1.month >0 and t1.month < 13 then
         -- Pad single digit months
         if #tostring(t1.month) == 1 then
            t2.month = string.format("0%s", tostring(t1.month))
         elseif #tostring(t1.month) == 2 then
            t2.month = tostring(t1.month)
         end
      else return nil, "Month should be a number 1-12" 
      end
   else return nil, "Field month is required and should be 1-12" 
   end
   if t1.day then
      if type(t1.day) == "number" then
         if t1.day > 9 then
            t2.mday = tostring(t1.day)
         else t2.mday = string.format("0%s", tostring(t1.day))
         end
      else return nil, "Field day is required to be a number between 1-31" 
      end
   else return nil, "Field day is required and should be 1-31"  
   end
   if t1.hour then
      if type(t1.hour) == "number" and t1.hour < 24 then
         if t1.hour > 9 then
            t2.hour = tostring(t1.hour)
         else t2.hour = string.format("0%s", tostring(t1.hour))
         end
      else return nil, "Field hour is required and should be a number 0-23" 
      end
   end
   if t1.min then
      if type(t1.min) == "number" and t1.min < 60 then
         if t1.min > 9 then
            t2.minute = tostring(t1.min)
         else t2.minute = string.format("0%s", tostring(t1.min))
         end
      else return nil, "Field min (for minute) is required and should be 0-59" 
      end
   else return nil, "Field min (for minute) is required and should be 0-59"
   end
   -- Seconds is not required
   t2.second = ""
   if t1.sec and type(t1.sec) == "number" then
      if t1.sec < 10 then
         t2.second = string.format(":0%s", tostring(t1.sec))  
      else t2.second = tostring(t1.sec)
      end
   end
   o.date = (string.format("%s-%s-%sT%s:%s%sZ", t2.fullyear, t2.month, t2.mday, t2.hour, t2.minute, t2.second))
   return o
end

-- AS Object definitions

-- Used in generating summaries
local who, what, didWhat, fromWhat, toWhat

as.Object = as:new()
as.Object.isObject = true

as.Link = as:new()
as.Link.isLink = true

as.Activity = as.Object:new()
as.Activity.isActivity = true
as.Activity.action = "performed an activity"

function as.Activity.new(self, o, jsonIn)
    o = as.object:new(o, jsonIn)
    o = o[o.type]:buildSummary()
    return o
end

function as.Activity.buildSummary(self)
    if o.summary == nil then
        if o.Actor and o.actor.name then
            who = o.actor.name or o:getID("actor")
        end
        if o.object then
            toWhat = o.object.name or o.object.summary or o:getID("object")
        end
        didWhat = self.action
    end
    if who and toWhat then
        o.summary = string.format( "%s %s on %s", who, didWhat, toWhat )
    end
    who = nil
    didWhat = nil
    toWhat = nil
    return o
end


as.IntransitiveActivity = as.Activity:new()
as.IntransitiveActivity.isIntransitiveActivity = true
function as.IntransitiveActivity.new(self, o, jsonIn)
    o = as.Activity:new(o, jsonIn)
end

function as.IntransitiveActivity.buildSummary(self)
    if o.summary = nil and o.actor then
        who = o.actor.name or as.getID()
        didWhat = self.action
        if who and didWhat then
            o.summary = string.format( "%s %s", who, didWhat )
        end
        who = nil
        didWhat = nil
    end
end

as.Collection = as.Object:new()
as.Collection.isCollection = true
as.Collection.items = {}
as.Collection.addTo = "items"
as.Collection.totalItems = 0
as.Collection.first = {}
as.Collection.last = {}
as.Collection.current = 0
-- Adds an Object or Link to the Collection
function as.Collection:add(o)
   -- Check that addition is well formed
   if o.type then
     local tmp = as[o.type]:new(o)
     if tmp then
       o = tmp
     else
       o = as.Object:new(o)
   else
     o = as.Object:new(o)
   end
   -- Increment totalItems
   self.totalItems = self.totalItems + 1
   -- Make new item last in list
   self.last = as.getID(o) or o
   -- Insert item 
   table.insert(self, o)
   return self
end
-- Removes an Object or Link to the Collection
function as.Collection:remove(o)
    -- Check that item is present
    local id = o:getID()
    for k, v in pairs(self) do
        if id == as.getID(v) then
            self.totalItems = self.totalItems - 1
            table.remove(o, k)
            local newLast = table.maxn(self)
            self.last = as.getID(newLast) or newLast
        end
    end
    return self
end

as.OrderedCollection = as.Collection:new()
as.OrderedCollection.isOrderedCollection = true
as.OrderedCollection.addTo = "orderedItems"

as.CollectionPage = as.Collection:new()
as.CollectionPage.isCollectionPage = true

as.OrderedCollectionPage = lutils.createClass(as.OrderedCollection, as.CollectionPage)
as.OrderedCollectionPage.isOrderedCollectionPage = true


as.Actor = as.Object:new()
as.Actor.isActor = true
function as.Actor.new(self, o, jsonIn)
    o = as.Object:new(o, jsonIn)
end

as.Accept = as.Activity:new()
as.Accept.action = "accepted"

as.Add = as.Activity:new()
as.Add.action = "added"

as.Announce = as.Activity:new()
as.Announce.action = "announced"

as.Arrive = as.IntransitiveActivity:new()
as.Arrive.action = "arrived"

as.Block = as.Activity:new()
as.Block.action = "blocked"

as.Create = as.Activity:new()
as.Create.action = "created"

as.Delete = as.Activity:new()
as.Delete.action = "deleted"

as.Dislike = as.Activity:new()
as.Dislike.action = "disliked"

as.Flag = as.Activity:new()
as.Flag.action = "flagged"

as.Follow = as.Activity:new()
as.Follow.action = "followed"

as.Ignore = as.Activity:new()
as.Ignore.action = "ignored"

as.Invite = as.Offer:new()
as.Invite.action = "invited"

as.Join = as.Activity:new()
as.Join.action = "joined"

as.Leave = as.Activity:new()
as.Leave.action = "left"

as.Like = as.Activity:new()
as.Like.action = "liked"

as.Listen = as.Activity:new()
as.Listen.action = "listened"

as.Move = as.Activity:new()
as.Move.action = "moved"
function as.Move.buildSummary(self)
    if self.summary == nil then
        if self.Actor and self.actor.name then
            who = self.actor.name or self:getID("actor")
        end
        if self.object then
            toWhat = self.object.name or self.object.summary or self:getID("object")
        end
        didWhat = self.action
    end
    if who and toWhat then
        local fromWhere = "a Collection"
        if self.origin then
            fromWhere = self.origin.name or self.origin:getID("origin") or fromWhere
        end
        local toWhere = "another Collection"
        if self.target then
            fromWhere = self.target.name or self.origin:getID("target") or toWhere
        end
        self.summary = string.format( "%s %s on %s", who, didWhat, toWhat )
    end
    who = nil
    didWhat = nil
    toWhat = nil
    return o
end
        
as.Offer = as.Activity:new()
as.Offer.action = "offered"

as.Question = as.Activity:new()
as.Question.action = "asked"

as.Reject = as.Activity:new()
as.Reject.action = "rejected"

as.Read = as.Activity:new()
as.Read.action = "read"

as.Remove = as.Activity:new()
as.Remove.action = "removed"

as.TentativeAccept = as.Accept:new()
as.TentativeAccept.action = "tentatively accepted"

as.TentativeReject = as.Reject:new()
as.TentativeReject.action = "tentatively rejected"

as.Travel = as.Activity:new()
as.Travel.action = "traveled"

as.Undo = as.Activity:new()
as.Undo.action = "undid"

as.Update = as.Activity:new()
as.Update.action = "updated"

as.View = as.Activity:new()
as.View.action = "viewed"

-- Actor types are extremely vague in AS
-- Since AP further defines the structure of Actors
-- it's useful to defer defining these until implementation
as.Application = as.Actor:new()
as.Group = as.Actor:new()
as.Organization = as.Actor:new()
as.Person = as.Actor:new()
as.Service = as.Actor:new()


as.Article = as.Object:new()
as.Document = as.Object:new()
as.Event = as.Object:new()
as.Note = as.Object:new()
as.Page = as.Object:new()
as.Place = as.Object:new()
as.Profile = as.Object:new()
as.Relationship = as.Object:new()
as.Tombstone = as.Object:new()
as.Video = as.Object:new()

as.Audio = as.Document:new()
as.Image = as.Document:new()


as.Mention = as.Link:new()



--[[
--
-- This functionality will probably be rewritten 
-- to make the most of Lua's object capabilities
--
--
--
-- This function emits an activity wrapped object based on the source doc
-- While it's important to emit valid C2S or S2S depending on the target
-- It's less important to differentiate them in incoming messages
function as.normalize(src, callDepth)
   -- src is an AS object
   -- callDepth is our recursion level
   callDepth = callDepth or 0


   -- If type is a supported activity, 
   -- we can process as an activity
   for _, v in pairs(as.supportedActivities) do
      if src.type == v then
         local activity, object, e = as.normalizeActivity()
         return activity, object, e
      end
   end

   -- If type is a supported object, 
   -- we can process as an object
   for _, v in pairs(as.supportedObjects) do
      if src.type == v then
         local activity, object, e = as.normalizeObject()
         return activity, object, e
      end
   end


   -- If type is a supported link,
   -- we can process as a link
   for _, v in pairs(as.supportedLinks) do
      if src.type == v then
         local activity, object, e = as.normalizeLink()
         return activity, object, e
      end
   end
end
   

function as.normalizeActivity(src, callDepth)
   src = src or ap.new{}
   callDepth = callDepth or 0
   -- An activity is only useful if the object is known
   if as.getID(src) == nil and as.getID(src.object) == nil then
      if type(src.object) == "table" then
         src.object = as.normalize(src.object, callDepth+1)
         -- TODO check this
         src.object.attributedTo = as.getID(src, "Actor")
         return src
      else 
         e = string.format("%s activity without an object", src.type)
         return nil, e
      end
   -- An Object is attached or referenced
   elseif type(as.getID(src.object)) == "string" then
      -- Object is attached
      if type(src.object) == "table" then
         src.object = ap.new(src.object)
         -- TODO check this
         src.object.attributedTo = as.getID(src, "Actor")
                return src
         -- Object is referenced
      elseif type(srcObject) == "string" then
         return src
      else 
         e = string.format("%s activity has weird Object ID", src.type)
         return nil, e
      end
   end
end


function as.normalizeObject(src, callDepth)
   --Object normalization
   src = src or ap:new()
   if callDepth == 0 then
      -- When an Object that isn't an Activity is at root
      -- We wrap it with a Create Object
      local activity = ap:new()
      activity.type = "Create"
      activity.object = as.normalizeObject(src, callDepth+1)
      return activity
   elseif type(src) == "Table" then
      -- TODO (normalize)
   elseif type(src) == "String" then
      -- TODO (fetch and normalize)
   else -- TODO (error)
   end

end


function as.normalizeLink(src)
   -- Link normalization
   src = src or ap:new()
   if callDepth == 0 then
      -- When a Link is at root we wrap it with a Create Object
      local activity = ap:new()
      activity.type = "Create"
      activity.object = as.normalizeObject(src, callDepth+1)
      return activity
   elseif type(src) == "Table" then
      -- TODO (normalize)
   elseif type(src) == "String" then
      -- TODO (fetch and normalize)
   else -- TODO (error)
   end
end
      

as.supportedActivities = {
   "Accept",
   "Add",
   "Announce",
   "Arrive",
   "Block",
   "Create",
   "Delete",
   "Dislike",
   "Flag",
   "Follow",
   "Ignore",
   "Invite",
   "Join",
   "Leave",
   "Like",
   "Listen",
   "Move",
   "Offer",
   "Question",
   "Reject",
   "Read",
   "Remove",
   "TentativeReject",
   "TentativeAccept",
   "Travel",
   "Undo",
   "Update",
   "View",
}

as.supportedObjects = {
   "Article",
   "Audio",
   "Document",
   "Event",
   "Image",
   "Note",
   "Page",
   "Place",
   "Profile",
   "Relationship",
   "Tombstone",
   "Video",
}

as.supportedLinks = {
   "Mention",
}
--]]
--
--
--
