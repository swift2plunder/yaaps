#!/usr/bin/env lua5.1
-- YAAPS, Yet Another ActivityPub Server
-- Copyright (C) 2019 by C. Babcock
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- Standard lbraries
require("os")
require("string")

-- Our libraries
require("lutils")
require("ap")

-- Installed libraries
local json = require("yaml")

-- GET is the only request method allowed for this endpoint
method = os.getenv( "REQUEST_METHOD" )
if method == "POST" then
   -- Validate connection as owner of the actor
   -- Process incoming document
   -- Provide feedback to connected client
elseif method == "GET" then
   -- Verify identity of connected client
   -- Perform any blocklist or other permission checks
   -- Serve outbox contents to connected client
else
    io.write( "Status: 405 'Method Not Allowed'\n" )
end
os.exit(0)

--[[
-- This is to prevent people from probing the filesystem
local path = os.getenv("REQUEST_URI")
-- The double path separator is an attempt to reference from root
-- Dot before path is an attempt to use relative paths
if string.match( path, "//" ) or string.match( path, "%./" ) then
    io.write( "\n" )
    os.exit(0)
end


local root = os.getenv("DOCUMENT_ROOT")
local source = string.gsub( string.format( "%s%s", root, path ) , "\.actor$", "actor.lua" )
local object, e = dofile( source ) 
if e then
    io.write( "Status: 404 'File Not Found'\n" )
    os.exit(0)
end

-- We can do additional processing
-- Convert to JSON
local response = json.encode(object)

-- Output
io.write( string.format( "Status: 200\n\n%s", response )  )

--]]
